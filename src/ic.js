﻿var ic747=function(canvasid,x,y)
{
    this.x = x;
    this.y = y;
    this.ip = [];
    this.op = [];
    this.iplocation = [];//x and y
    this.ipcolor = [];//color
    this.oulocation = [];//x an y
    this.oucolor = [];//color
    this.outputvalue = [];//1 1 1 0 0 0
    this.oplinkgate = [];//gate and ipno
    this.canvas = document.getElementById(canvasid);
    this.context = this.canvas.getContext("2d");
    this.inputno=8;
    this.outputno=8;
    for(i=0;i<=7;i++)
    {
        this.ip[i] = 0;
        this.ipcolor.push({ color: "grey" });
        this.oucolor.push({ color: "grey" });
    }

    this.setiplocation();
    this.setoulocation();
    
}

ic747.prototype.setip=function(ipno,value)
{
    this.ip[ipno] = value;
    if (value == 1)
    {
        this.ipcolor[i].corlor="green";
    }
    else
    {
        this.ipcolor[i].corlor = "grey";
    }
    this.output();
}

ic747.prototype.output=function()
{
    this.setoutput();
    this.setgateoutput();
}

ic747.prototype.setgateoutput=function()
{
    for(i=0;i<this.oplinkgate.length;i++)
    {
        this.oplinkgate[i].gate.setip(this.oplinkgate[i].ipno,this.outputvalue[this.inputno+i]);
    }
}

ic747.prototype.outlink=function(gate,ipno)
{
    if(this.oplinkgate.length<this.outputno-1)
    {
        this.oplinkgate.push({gate:gate,ipno:ipno});
    }
    else {
        alert("all links are busy");
    }
}

ic747.prototype.draw=function()
{
    this.drawip();
    this.drawou();
    this.drawgate();
}

ic747.prototype.setiplocation=function()
{
    var x = this.x-12.5;
    var y = this.y + 10;
    for(i=1;i<=this.inputno;i++)
    {
        
        this.iplocation.push({ x: x - 7, y: y });
        y = y + 15;
    }
}
ic747.prototype.drawip=function()
{
    var x = this.x - 12.5;
    var y = this.y + 10;
    for (i = 1; i <= this.inputno; i++) {
        this.context.beginPath();
        this.context.arc(x - 7, y, 5, 0, 2 * Math.PI, false);
        this.context.lineWidth = 1;
        this.context.strokeStyle = "black";
        this.context.fillStyle = this.ipcolor[i - 1].color;
        this.context.fill();
        this.context.stroke();

        this.context.beginPath();
        this.context.moveTo(x - 7, y);
        this.context.lineTo(x, y);
        this.context.lineWidth = 2;
        this.context.strokeStyle = this.ipcolor[i - 1].color;

        this.context.stroke();
        
        y = y + 15;
    }
}

ic747.prototype.setoulocation=function()
{
    var x = this.x + 12.5;
    var y = this.y + 10;
    for (i = 1; i <= this.outputno; i++) {

        this.iplocation.push({ x: x + 7, y: y });
        y = y + 15;
    }
}

ic747.prototype.drawou = function () {
    var x = this.x + 12.5;
    var y = this.y + 10;
    for (i = 1; i <= this.outputno; i++) {
        this.context.beginPath();
        this.context.arc(x + 7, y, 5, 0, 2 * Math.PI, false);
        this.context.lineWidth = 1;
        this.context.strokeStyle = "black";
        this.context.fillStyle = this.oucolor[i - 1].color;
        this.context.fill();
        this.context.stroke();

        this.context.beginPath();
        this.context.moveTo(x + 7, y);
        this.context.lineTo(x, y);
        this.context.lineWidth = 2;
        this.context.strokeStyle = this.oucolor[i - 1].color;

        this.context.stroke();

        y = y + 15;
    }
}

ic747.prototype.drawgate = function () {
    var max;
    if (this.inputno < this.outputno)
    {
        max = 15 * this.outputno + 10;
    }
    else
    {
        max = 15 * this.inputno + 10;
    }
        this.context.beginPath();
        this.context.moveTo(this.x , this.y);
        this.context.lineTo(this.x - 12.5, this.y);
        this.context.lineTo(this.x - 12.5, this.y + max);
        this.context.lineTo(this.x + 12.5, this.y + max);
        this.context.lineTo(this.x + 12.5, this.y);
        this.context.lineTo(this.x , this.y);
        this.context.lineWidth = 1;
        this.context.strokeStyle = "black";

        this.context.stroke();

}